import React from 'react';
import './App.css';

const Circle = () => <circle cx="5" cy="5" r="5"/>;
const Square = () => <rect x="0" y="0" width="10" height="10"/>;
const Triangle = () => <polygon points="0,0 5,10 10,0"/>
const Star = () => <polygon points="108.85714,151.10119 82.443152,136.14677 55.149564,149.4281 61.209691,119.68573 40.144218,97.832139 70.303572,94.404765 84.577982,67.617179 97.157361,95.241313 127.0449,100.53926 104.66003,121.03929" transform="matrix(0.11281667,0,0,0.11752223,-4,-8)"/>;


// translate(-220 -163) scale(2.5)

const Shape = ({name}) => {
  switch (name) {
  case 'circle': return Circle();
  case 'square': return Square();
  case 'triangle': return Triangle();
  case 'star': return Star();
  default: return <text>Unknown shape</text>;
  }
}

class App extends React.Component {
  constructor() {
    super();
    this.state = {'shape': 'circle', 'colour': '#ff0000'};
    this.changeShape = this.changeShape.bind(this);
    this.changeColour = this.changeColour.bind(this);
  }

  changeShape(event) {
    this.setState({shape: event.target.value});
  }
  changeColour(event) {
    this.setState({colour: event.target.value});
  }
  
  render() {
    return (
        <div className="App">
          <div className="menu">
            <select value={this.state.shape} onChange={this.changeShape}>
              <option value="circle">Circle</option>
              <option value="square">Square</option>
              <option value="triangle">Triangle</option>
              <option value="star">Star</option>
            </select>
            <select value={this.state.colour} onChange={this.changeColour}>
              <option value="#ff0000">Red</option> 
              <option value="#ff8800">Orange</option>
              <option value="#ffff00">Yellow</option>
              <option value="#00ff00">Green</option>
              <option value="#0000ff">Blue</option>
              <option value="#cc11ff">Purple</option> 
           </select>
          </div> 
            <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox="0 0 10 10" className="gfx">
              <g fill={this.state.colour}>
                <Shape name={this.state.shape}/>
              </g>
            </svg>
        </div>
    );
  }
}

export default App;
